///<reference types ="Cypress"/>
describe(('API testing'),()=>{
    //Overriding baseurl for this file only
    
    Cypress.config('baseUrl','https://jsonplaceholder.typicode.com/')

    
    
    it('GET-read',()=>{
        cy.request('/posts').then((response)=>{
            expect(response).to.have.property('status',200)
            expect(response.body).to.not.be.null
            expect(response.body).to.have.length(100)
        
        })
         cy.request('/posts/1').as('post')
         cy.get('@post').its('status').should('equal',200)

    })

     it('POST-create',()=>{
         const item={"id":3,"title":"abc","body":"asdff"}
          cy.request('POST','/posts',item).then((resp)=>{
            expect(resp).to.have.property('body')
            expect(resp).to.have.property('status',201)
          })
          
     
    }) 
    it('PUT-update',()=>{
        cy.request({method:'PUT',
        url:'/posts/3',
        body:
        {
            "id":3,
            "title":"AAA",
            "body":"asdff"
        }
    }).its(body).should('include',{title : 'AAA'})
    
    })
    it('DELETE',()=>{
        cy.request('DELETE','/posts')
        cy.request('/posts').then((resp)=>{
            expect(resp.body).to.be.null
        })
    })
})