///<reference types="Cypress"/>
describe(' Handling Dymanic content',()=>{
    it('Dynamic Welcome message',()=>{  
      
      cy.login('Admin','admin123')
      cy.url().should('include','dashboard')

      cy.get('#welcome').then(function($welcomeTxt){
           const msg = $welcomeTxt.text()
            cy.log(msg)
            //   expect(msg).eq('Welcome'+' Admin')
            expect(msg).to.match(/Welcome .*/)
            cy.contains(/Welcome .*/).click()
            cy.get('#welcome-menu').contains('Logout').click()
            cy.url().should('include','auth')
      })  
    })
})