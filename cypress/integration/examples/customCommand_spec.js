///<reference types="Cypress"/>
describe('Custom Suite',()=>{
    it('login functionality using custom command',()=>{

        cy.login('Admin','admin')//invalid password
        cy.url().should('include','auth')
    
        cy.login('user','admin123')//invalid username
        cy.url().should('include','auth')

        cy.login('user','admin')//invlalid
        cy.url().should('include','auth')

        cy.login('Admin','admin123')//valid
        cy.url().should('include','dashboard')

    })
})
