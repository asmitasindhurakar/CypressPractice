
///<reference types = "Cypress"/>

describe("Verify Login Functionalities using fixtures data",()=>{
   
    beforeEach(()=>{
        cy.visit('https://opensource-demo.orangehrmlive.com/')
        cy.get('#txtUsername').should('be.enabled').as('user')
        cy.get('#txtPassword').should('be.enabled').as('password')
        cy.get('#btnLogin').should('be.visible').as('btn')
    })   
    
    it("Valid username and password",()=>{
        cy.fixture('credentials').then(function(data){
        this.data = data
        cy.get('@user').type(this.data[0].user)
        cy.get('@password').type(this.data[0].password)
       })       
        cy.get('@btn').click()
        cy.url().should('include','dashboard')
        
        //GET method
        cy.request('/dashboard').then((response)=>{
            expect(response).to.have.property('status',200)})
        
    })
    it("Valid username and Invalid password",()=>{
        cy.fixture('credentials').then(function(data){
            this.data = data
         cy.get('@user').type(this.data[0].user)
         cy.get('@password').type(this.data[0].invalidpass)
        })
        cy.get('@btn').click()
        cy.url().should('include','auth')
        cy.get('#spanMessage').contains('Invalid credentials')
        cy.wait(2000)
    })
    it("Invalid username and Invalid password",()=>{
        cy.fixture('credentials').then(function(data){
            this.data = data
            cy.get('@user').type(this.data[0].invaliduser)
            cy.get('@password').type(this.data[0].invalidpass)
        })
        cy.get('@btn').click()
        cy.url().should('include','auth')
        cy.get('#spanMessage').contains('Invalid credentials')
    })
    it("Invalid username and valid password",()=>{
        cy.fixture('credentials').then(function(data){
            this.data = data
            cy.get('@user').type(this.data[0].invaliduser)
            cy.get('@password').should('be.enabled').type(this.data[0].password)
        })    
        cy.get('@btn').click()
        cy.url().should('include','auth')
        cy.get('#spanMessage').contains('Invalid credentials')
    })
    it("Password field is empty",()=>{
        cy.fixture('credentials').then(function(data){
            this.data = data
            cy.get('@user').type(this.data[0].invaliduser)
            cy.get('@password').should('be.enabled')
        })  
        cy.get('@btn').click()
        cy.get('#spanMessage').contains('Password cannot be empty')  
    })
    it("checks whether the forget password link works or not",()=>{
        cy.get('a').then(link =>{
            cy.request(link.prop('href'))
            .its('status')
            .should('eq',200)
        })
       
    })
})