///<reference types ="Cypress"/>


describe('HandlingGUI_Elements',()=>{
    before(()=>{
        cy.visit("http://demo.automationtesting.in/Register.html")
        // cy.on('uncaught:exception', (err, runnable) => {
        //  return false  
        // }) 
    })
    it(('input box'),()=>{
        cy.get('input[ng-model=FirstName]').should('be.visible').type("Dean")
        cy.get('input[ng-model=LastName]').should('be.visible').type("Winchester")
        cy.get('[ng-model=Adress').should('be.visible').type('ktm')
        cy.get('[ng-model=EmailAdress]').should('be.visible').type('dean@gmail.com')
        cy.get('[ng-model=Phone]').should('be.visible').type('9837123456')
        
   })
    it(('radio-buttons'),()=>{  
      cy.get('input[value=Male]').click().should("be.checked")  
      cy.get('input[value=FeMale]').should("not.be.checked")  
    })
    it(('checkbox'),()=>{
        //individual 
        cy.get('#checkbox1').check().should('be.checked')
        cy.get('#checkbox3').uncheck().should('not.be.checked')
        
        //multiple
        cy.get('input[type=checkbox]').check(['Cricket','Hockey'])
    })
    it('dropdowns',()=>{
        //single select
        cy.get('#Skills').select("Certifications").should('have.value','Certifications')
        cy.get('#countries').should('be.enabled')

        //multiple select

        cy.get('#msdd').click()
        cy.get('.ui-corner-all').contains('English').click()
        cy.get('.ui-corner-all').contains('Japanese').click()
        cy.get('.ui-corner-all').contains('Korean').click()
        cy.get(":nth-child(2) > .ui-icon").click()
        
        //searchable
        cy.get(".select2-selection").click({force:true})//forcefully click
        cy.get(".select2-search__field").type('new')
        cy.get(".select2-search__field").type('{enter}')

        cy.get("#yearbox").select('1997')
        cy.get("[placeholder=Month]").select('October')
        cy.get("#daybox").select('31')
    })
})