///<reference types ="Cypress"/>


describe('Form suite',()=>{
    before(()=>{
        cy.visit("http://demo.automationtesting.in/Register.html")
        // cy.on('uncaught:exception', (err, runnable) => {
        //  return false  
        // }) 
    })
    it('email required',()=>{
        cy.fixture('form.json').then(function(data){
            this.data=data
            cy.get('input[ng-model=FirstName]').type(this.data[0].fname)
            cy.get('input[ng-model=LastName]').type(this.data[0].lname)
            cy.get('[ng-model=Adress').should('be.visible').type(this.data[0].address)
            cy.get('[ng-model=Phone]').should('be.visible').type(this.data[0].phone)
        })
            cy.get('#submitbtn').click()   
            cy.contains('Please fill out this field')  
    })
})
