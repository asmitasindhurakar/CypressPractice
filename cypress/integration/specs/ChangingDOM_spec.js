///<reference types="Cypress"/>
describe('changing DOM',()=>{
    beforeEach(()=>{
        cy.visit("http://todomvc-app-for-testing.surge.sh/")
        cy.get(".new-todo").type("Learn Cypress{enter}")
        cy.get(".new-todo").type("Learn Automation{enter}")
        cy.get(".new-todo").type("Watch KDrama{enter}")
    })

    it("making the delete button visible using invoke (visible when hovered)",()=>{
        cy.get(':nth-child(2) > .view > .destroy')
        .invoke('show')
        .click()
    })

  
    it("adding to class completed using invoke",()=>{
    
        cy.get('.todo-list li')
        .eq(1)
        .invoke('addClass','completed')
    
        cy.get('.todo-list li').should('have.length',3)
        //since it is added without changing
   })
   
   it.skip("using trigger",()=>{
    
     cy.get('.todo-list li')
        .eq(0)
        .trigger('keydown')
    
        cy.get(':nth-child(1) > .view > .destroy')
        .should('be.visible')
 })
})