///<reference types="cypress"/>

describe("todo Filters",()=>{
  
    beforeEach(()=>{
        cy.visit("http://todomvc-app-for-testing.surge.sh/")
        cy.get(".new-todo").type("Learn Cypress{enter}")
        cy.get(".new-todo").type("Learn Automation{enter}")
        cy.get(".new-todo").type("Watch KDrama{enter}")
        cy.get(':nth-child(2) > .view > .toggle').click()
    })

    it('show the active todos',()=>{
        cy.contains("Active").click()
        cy.get('.todo-list li').should('have.length',2)
    })

    it('show the completed todos',()=>{
        cy.contains("Completed").click()
        cy.get('.todo-list li').should('have.length',1)
    })
    
    it('show the all todos',()=>{
        cy.contains("All").click()
        cy.get('.todo-list li').should('have.length',3)
    })
})