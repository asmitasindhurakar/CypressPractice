/// <reference types= "Cypress"/>

import { todoPage } from "./PageObject/todo-object";

describe("TODO actions",()=>{
    const TodoPage = new todoPage()
    beforeEach(()=>{
        TodoPage.navigate()
        TodoPage.addTodo('Learn Cypress')
    })
   
    it("should add a new item to the todo list",()=>{
        
        cy.get("label").should('have.text','Learn Cypress')
    })
    it("should mark todo as completes",()=>{
        cy.get('.toggle').should('not.be.checked')
        //check condition on css property 
        cy.get('.toggle').click()
        cy.get('label').should("have.css","text-decoration-line",'line-through')
    })
    it("should clear the completed todo",()=>{
        cy.get('.toggle').click()
         // check condition on DOM
         cy.contains('Clear'). click()
         //checking that list has no items
         cy.get('.todo-list').should('not.have.descendants','li')
    })
})
